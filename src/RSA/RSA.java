package RSA;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class RSA 
{
    public static String crypto(String message, String exp, String mod)
    {
        BigBinary mes = BigBinary.fromString(message);
        
        BigBinary m = BigBinary.fromString(mod);
        
        BigBinary e = BigBinary.fromString(exp);
        
        BigBinary result = new BigBinary();
        
        result.ModExp(mes, e, m);
        
        return result.toString();
        
        
    }
    
}
