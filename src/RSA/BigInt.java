package RSA;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
import java.util.ArrayList;

public class BigInt {

    private ArrayList<Byte> num;

    public BigInt() {
        num = new ArrayList<Byte>();

    }

    public BigInt(int InitSize) 
    {
        num = new ArrayList<Byte>(InitSize);

        
    }
    
    public static BigInt fromString(String s)
    {
        BigInt b = new BigInt(s.length());
        
        for(int i = s.length()-1; i >= 0; i--)
        {
            char c = s.charAt(i);
            if(c == '0')
            {
                b.num.add(Byte.valueOf((byte)0));
            }
            else if (c == '1')
            {
                b.num.add(Byte.valueOf((byte)1));
            }
            else
            {
                return null;
            }
        }
        
        return b;
        
    } 

    public void Product(BigInt multiplier, BigInt multiplicand) {
     
        

        for (int i = 0; i < multiplier.size(); i++) 
        {

           if (multiplier.num.get(i) == 1) 
           {
               this.add(multiplicand, i);
           }

        }
        

    }
    
    public void add(BigInt adder)
    {
        while(this.size() <= adder.size())
            {
                this.num.add((byte)0);
            }
        
        byte carry = 0;
        byte res;
        for(int i = 0; i < adder.size(); i++)
        {

            
            res = (byte)(this.num.get(i).byteValue() + adder.num.get(i) + carry);  
            carry = 0;
            
            if(res > 1)
            {
                res -= 2;
                carry++;
            }
            
            this.num.set(i, Byte.valueOf(res));
        }
        
        this.num.set(adder.size(), Byte.valueOf(carry));
        
    }
    
    public void add(BigInt adder, int shift)
    {
        while(this.size() <= adder.size() + shift)
            {
                this.num.add((byte)0);
            }
        
        byte carry = 0;
        byte res;
        
        
        for(int i = 0; i < adder.size(); i++)
        {
            
            
            res = (byte)(this.num.get(i+shift).byteValue() + adder.num.get(i) + carry);  
            carry = 0;
            
            if(res > 1)
            {
                res -= 2;
                carry++;
            }
            
            this.num.set(i+shift, Byte.valueOf(res));
        }
        
        this.num.set(adder.size() + shift, Byte.valueOf(carry));
        
    }
    
    public void subtract(BigInt Minus)
    {
       while(this.size() < Minus.size())
       {
                this.num.add((byte)0);
       }
       
        for(int i = 0; i < Minus.size(); i++)
        {
            byte res = (byte)(this.num.get(i) - Minus.num.get(i));
            
            if(res < 0)
            {
                res = 1;
                
                int mod = 0;
                while(this.num.get(mod+i).byteValue() == (byte)0)
                {
                    this.num.set(mod + i, Byte.valueOf((byte)1));
                    mod ++;
                    if(mod + i >= this.size())
                    {
                        return;
                    }                      
                }
                this.num.set(mod+i, Byte.valueOf((byte)0));
            }
            
            this.num.set(i,Byte.valueOf(res));
            
        }
        
     
        
    }
    
    public void subtract(BigInt Minus, int shift)
    {
       while(this.size() < Minus.size() + shift)
       {
                this.num.add((byte)0);
       }
       
        for(int i = 0; i < Minus.size(); i++)
        {
            byte res = (byte)(this.num.get(i+shift) - Minus.num.get(i));
            
            if(res < 0)
            {
                res = 1;
                
                int mod = shift;
                while(this.num.get(mod+i).byteValue() == (byte)0)
                {
                    this.num.set(mod + i, Byte.valueOf((byte)1));
                    mod ++;
                    if(mod + i >= this.size())
                    {
                        return;
                    }                      
                }
                this.num.set(mod+i, Byte.valueOf((byte)0));
            }
            
            this.num.set(i+shift,Byte.valueOf(res));
            
        }
        
     
        
    }
    
    public int compareTo(BigInt b)
    {
        int mSize = Math.max(this.size(), b.size());
        
                while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
         while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
        for (int i = mSize-1; i > 0; i--)
        {
            int compA = 0;
            if( i < this.size())
            {
                compA = (int)this.num.get(i);                
            }
            
            int compB = 0;
            if(i < b.size())
            {
                compB = (int)b.num.get(i);                
            }
            
            if(compA > compB)
            {
                return 1; 
            }
            else if (compB > compA)
            {
                return -1;                
            }
            
        }
        
        return 0; //Equal
        
        
        
    }
    
    public int compareTo(BigInt b, int shift)
    {
        int mSize = Math.max(this.size(), b.size());
        
        while(this.size() < mSize + shift)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
         while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
        for (int i = mSize-1; i > 0; i--)
        {
            int compA = 0;
            if( i < this.size())
            {
                compA = (int)this.num.get(i + shift);                
            }
            
            int compB = 0;
            if(i < b.size())
            {
                compB = (int)b.num.get(i);                
            }
            
            if(compA > compB)
            {
                return 1; 
            }
            else if (compB > compA)
            {
                return -1;                
            }
            
        }
        
        return 0; //Equal
        
        
        
    }
    
    public String toString()
    {
        boolean write = false;
        String s = "";
        
        for(int i = this.size() - 1; i >= 0; i-- )
        {
            if(this.num.get(i).byteValue() == (byte)1) write = true;
            
            if(write)
            {
                s +=  this.num.get(i).toString();  
            }
        }
        
        return s;
        
    }
    
    public BigInt QuotientMod(BigInt Top, BigInt Bottom)  //Returns Rem, this becomes Quotient
    {

            BigInt Rem = new BigInt();
            Rem.copyNum(Top);
            this.copyNum(recurDiv(Rem, Bottom, 0));
            
            return Rem;
        
    }
    
    private BigInt recurDiv(BigInt Top, BigInt Bottom, int shift) //Destructive to top, use a clone.  Top becomes remainder
    {
        int comp = Top.compareTo(Bottom, shift);
        
        if(comp < 0) //If the top is less than the bottom
        {
            return new BigInt(0);
        }
        
        BigInt two = BigInt.fromString("10");
        BigInt one = BigInt.fromString("1");
        
        BigInt quo = new BigInt();
        
        quo.Product(two, recurDiv(Top, Bottom, shift+1));
        
        
        
        comp = Top.compareTo(Bottom, shift);
        
        if(comp >= 0)
        {
            quo.add(one);
            Top.subtract(Bottom, shift);
        }
        
        System.out.println(quo);
        
        return quo;
    }
    
    
    public int size()
    {
        return num.size();        
    }
    
    public void copyNum(BigInt b)
    {
        this.num.clear();
        this.num.addAll(b.num);        
    }
}
