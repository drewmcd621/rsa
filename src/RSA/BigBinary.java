package RSA;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
import java.util.ArrayList;

public class BigBinary {

    private ArrayList<Byte> num;
    
    //Preformance variables
    public long CALLS_MODEXP;
    public long CALLS_ADD;
    public long CALLS_SUB;
    public long CALLS_MOD;
    public long CALLS_MUL;
    public long CALLS_DIV;
    public long CALLS_COMP;

    public BigBinary() {
        num = new ArrayList<Byte>();

    }

    public BigBinary(int InitSize) 
    {
        num = new ArrayList<Byte>(InitSize);

        
    }
    
    public static BigBinary fromString(String s)
    {
        BigBinary b = new BigBinary(s.length());
        
        for(int i = s.length()-1; i >= 0; i--)
        {
            char c = s.charAt(i);
            if(c == '0')
            {
                b.num.add(Byte.valueOf((byte)0));
            }
            else if (c == '1')
            {
                b.num.add(Byte.valueOf((byte)1));
            }
            else
            {
                return null;
            }
        }
        
        return b;
        
    } 
    
    public void add(BigBinary adder)
    {
        CALLS_ADD = 0;
        
        while(this.size() <= adder.size())
            {
                this.num.add((byte)0);
            }
        
        byte carry = 0;
        byte res;
        for(int i = 0; i < adder.size(); i++)
        {
            CALLS_ADD++;

            
            res = (byte)(this.num.get(i).byteValue() + adder.num.get(i) + carry);  
            carry = 0;
            
            if(res > 1)
            {
                res -= 2;
                carry++;
            }
            
            this.num.set(i, Byte.valueOf(res));
        }
        
        this.num.set(adder.size(), Byte.valueOf(carry));
        
    }
    
    public void add(BigBinary adder, int shift)
    {
        CALLS_ADD = 0;
        
        while(this.size() <= adder.size() + shift)
            {
                this.num.add((byte)0);
            }
        
        byte carry = 0;
        byte res;
        
        
        for(int i = 0; i < adder.size(); i++)
        {
            CALLS_ADD++;            
            
            res = (byte)(this.num.get(i+shift).byteValue() + adder.num.get(i) + carry);  
            carry = 0;
            
            if(res > 1)
            {
                res -= 2;
                carry++;
            }
            
            this.num.set(i+shift, Byte.valueOf(res));
        }
        
        this.num.set(adder.size() + shift, Byte.valueOf(carry));
        
    }
    
        public void Product(BigBinary multiplier, BigBinary multiplicand) 
    {
        CALLS_MUL = 0;
        BigBinary res = new BigBinary();
        
        res = new BigBinary();

        for (int i = 0; i < multiplier.size(); i++) 
        {
            CALLS_MUL++;

           if (multiplier.num.get(i) == 1) 
           {
               res.add(multiplicand, i);
               
               CALLS_MUL += res.CALLS_ADD;
           }
           

        }
        
        this.copyNum(res);
        

    }
    
    public void subtract(BigBinary Minus)
    {
        CALLS_SUB=0;
       while(this.size() < Minus.size())
       {
                this.num.add((byte)0);
       }
       
        for(int i = 0; i < Minus.size(); i++)
        {
            CALLS_SUB++;
            
            byte res = (byte)(this.num.get(i) - Minus.num.get(i));
            
            if(res < 0)
            {
                res = 1;
                
                int mod = 0;
                while(this.num.get(mod+i).byteValue() == (byte)0)
                {
                    CALLS_SUB++;
                    
                    this.num.set(mod + i, Byte.valueOf((byte)1));
                    mod ++;
                    if(mod + i >= this.size())
                    {
                        return;
                    }                      
                }
                this.num.set(mod+i, Byte.valueOf((byte)0));
            }
            
            this.num.set(i,Byte.valueOf(res));
            
        }
        
     
        
    }
    
    public void subtract(BigBinary Minus, int shift)
    {
        CALLS_SUB = 0;
        
       while(this.size() < Minus.size() + shift)
       {
                this.num.add((byte)0);
       }
       
        for(int i = 0; i < Minus.size(); i++)
        {
            CALLS_SUB++;
            
            byte res = (byte)(this.num.get(i+shift) - Minus.num.get(i));
            
            if(res < 0)
            {
                res = 1;
                
                int mod = shift;
                while(this.num.get(mod+i).byteValue() == (byte)0)
                {
                    CALLS_SUB++;
                    this.num.set(mod + i, Byte.valueOf((byte)1));
                    mod ++;
                    if(mod + i >= this.size())
                    {
                        return;
                    }                      
                }
                this.num.set(mod+i, Byte.valueOf((byte)0));
            }
            
            this.num.set(i+shift,Byte.valueOf(res));
            
        }
        
     
        
    }
    
    public BigBinary QuotientMod(BigBinary Top, BigBinary Bottom)  //Returns Rem, this becomes Quotient
    {
            CALLS_DIV=0;
            BigBinary Rem = new BigBinary();
            Rem.copyNum(Top);
            this.copyNum(recurDiv(Rem, Bottom, 0));
            return Rem;
        
    }
    
        private BigBinary recurDiv(BigBinary Top, BigBinary Bottom, int shift) //Destructive to top, use a clone.  Top becomes remainder
    {
        CALLS_DIV++;
        int comp = Top.compareTo(Bottom, shift);
        CALLS_DIV += Top.CALLS_COMP;
        
        if(comp < 0) //If the top is less than the bottom
        {
            return new BigBinary(0);
        }
        
        BigBinary two = BigBinary.fromString("10");
        BigBinary one = BigBinary.fromString("1");
        
        BigBinary quo = new BigBinary();
        
        
        quo.Product(two, recurDiv(Top, Bottom, shift+1));
        
        
        
        comp = Top.compareTo(Bottom, shift);
        CALLS_DIV += Top.CALLS_COMP;
        
        if(comp >= 0)
        {
            quo.add(one);
            Top.subtract(Bottom, shift);
            CALLS_DIV += Top.CALLS_SUB;
        }
        
        //System.out.println(quo);
        
        return quo;
    }
    
    public void Mod(BigBinary Top, BigBinary Bottom)  //This becomes remainder
    {
        CALLS_MOD = 0;
        BigBinary res = new BigBinary();
        res.copyNum(Top);
        recurRem(res, Bottom, 0);
        this.copyNum(res);

    }
    
    private void recurRem(BigBinary Top, BigBinary Bottom, int shift) //Destructive to top, use a clone.  Top becomes remainder
    {
        CALLS_MOD++;
        int comp = Top.compareTo(Bottom, shift);
        CALLS_MOD += Top.CALLS_COMP;
        
        Top.cleanBits();
        
        if(comp < 0) //If the top is less than the bottom
        {
            return;
        }
        
        if(shift == Integer.MAX_VALUE)
        {
            System.out.println("ERROR");
        }

        
        
        recurRem(Top, Bottom, shift+1);
        
        
        
        comp = Top.compareTo(Bottom, shift);
        CALLS_MOD += Top.CALLS_COMP;
        
        if(comp >= 0)
        {
            Top.cleanBits();

            Top.subtract(Bottom, shift);
            CALLS_MOD += Top.CALLS_SUB;
        }
        
        
    } 

    
    public void ModExp(BigBinary Base, BigBinary Exp, BigBinary Mod) //This becomes the value of Base^Exp (mod Mod)
    {
        CALLS_MODEXP = 0;
        BigBinary res = new BigBinary();
        res = recurModExp(Base, Exp, Mod, 0);
        this.copyNum(res);
    }
    
    private BigBinary recurModExp(BigBinary Base, BigBinary Exp, BigBinary Mod, int shift)
    {
        CALLS_MODEXP++;
        BigBinary zero = BigBinary.fromString("0");
        BigBinary one = BigBinary.fromString("1");
        
        BigBinary res = new BigBinary();
        
        if(Exp.compareTo(zero, shift) == 0)
        {
             res.copyNum(one); //Base case 
             return res;
        }
        CALLS_MODEXP += Exp.CALLS_COMP;
        
        if(Exp.compareTo(one, shift) == 0)
        {
            res.copyNum(Base); //Base case
            return res;
        }
        CALLS_MODEXP += Exp.CALLS_COMP;
        
        
        
        res =  recurModExp(Base, Exp,Mod, shift + 1); // (x^n) / 2
        
        res.Product(res, res); // ((x^n)/2)^2
        CALLS_MODEXP += res.CALLS_MUL;
        
        
        if(Exp.num.get(shift) == Byte.valueOf((byte)1))
        {
             res.Product(Base, res); // x * ((x^n)/2)^2
             CALLS_MODEXP += res.CALLS_MUL;
        }
        
        
        res.Mod(res, Mod);
        CALLS_MODEXP += res.CALLS_MOD;
        
        return res;
                
    }
    

    
    public int compareTo(BigBinary b)
    {
        CALLS_COMP = 0;
        
        int mSize = Math.max(this.size(), b.size());
        
                while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
         while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
        for (int i = mSize-1; i > 0; i--)
        {
            CALLS_COMP++;
            
            int compA = 0;
            if( i < this.size())
            {
                compA = (int)this.num.get(i);                
            }
            
            int compB = 0;
            if(i < b.size())
            {
                compB = (int)b.num.get(i);                
            }
            
            if(compA > compB)
            {
                return 1; 
            }
            else if (compB > compA)
            {
                return -1;                
            }
            
        }
        
        return 0; //Equal
        
        
        
    }
    
    public int compareTo(BigBinary b, int shift)
    {
        CALLS_COMP = 0;
        this.cleanBits();
        
        int mSize = Math.max(this.size(), b.size());
        
        
        
        while(this.size() < mSize + shift)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
         while(this.size() < mSize)
        {
            this.num.add(Byte.valueOf((byte)0));            
        }
        
        for (int i = mSize-1; i >= 0; i--)
        {
            CALLS_COMP++;
            int compA = 0;
            if( i < this.size())
            {
                compA = (int)this.num.get(i + shift);                
            }
            
            int compB = 0;
            if(i < b.size())
            {
                compB = (int)b.num.get(i);                
            }
            
            if(compA > compB)
            {
                return 1; 
            }
            else if (compB > compA)
            {
                return -1;                
            }
            
        }
        
        return 0; //Equal
        
        
        
    }
    
    public String toString()
    {
        this.cleanBits();
        
        String s = "";
        
        for(int i = this.size() - 1; i >= 0; i-- )
        {

                s +=  this.num.get(i).toString();  

        }
        
        return s;
        
    }
    
    
    
    public int size()
    {
        return num.size();        
    }
    
    public void copyNum(BigBinary b)
    {
        this.num.clear();
        this.num.addAll(b.num);        
    }
    
    private void cleanBits()
    {
        
        
        while(this.num.get(this.size() - 1) == Byte.valueOf((byte)0))
        {
            if(this.size() <= 1) return;
            this.num.remove(this.size() - 1);        
        }
        
    }
}
