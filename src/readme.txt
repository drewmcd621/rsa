adm75@pitt.edu
Drew McDermott

RSA cryptography

    This program is designed to encrypt and decrypt messages based on the RSA encryption
    method.  Currently the key must be in the form of a binary string.

Classes

    RSA - A simple class which sets up RSA cryptography.  RSA is static.

    BigBinary - Provides manipulation of large arrays of binary numbers.  
    
    Including:
        -Add
        -Subtract
        -Multiply
        -Divide
        -Remainder
        -Modulus Exponentiation

    RSA uses Modulus Exponentiation which is an O(n^3) where n is the length of the
    largest input.  Included is a test file called "Driver" which will analyze the time
    and calls of the ModExp function.  It creates a CSV file in the directory of the 
    program with the length in the first column and the time in ms taken in the other.

How to use

    Call RSA.crypto(String Message, String Exponent, String Modulus).  Where the strings
    are binary numbers (e.g. "1011011").  The function will return a binary string as 
    the result.

To do:

    -Create a converter to convert keys from several types into binary
    -Have RSA generate keys itself.