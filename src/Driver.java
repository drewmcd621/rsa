
/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
import RSA.*;
import java.io.*;
import java.util.Random;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Driver {

    static int MAX_LEN = 300;
    static int INTV = 10;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        
        try {
        FileWriter fw = new FileWriter("Results.csv");
        PrintWriter outF = new PrintWriter(fw);
        
                
        
        



        String a = "";
        String b = "";
        String c = "";

        long start;
        long end;
        
        outF.println("0,0");
        
        Random R = new Random();

        for (int i = INTV; i <= MAX_LEN;  i += INTV) 
        {
            
                for(int j = 0; j < INTV; j++)
                {
                    a += R.nextInt(2);
                    b += R.nextInt(2);
                    c += R.nextInt(2);
                }

                start = System.currentTimeMillis();

                BigBinary mes = BigBinary.fromString(a);

                BigBinary m = BigBinary.fromString(b);

                BigBinary e = BigBinary.fromString(c);

                BigBinary result = new BigBinary();

                result.ModExp(mes, e, m);

                end = System.currentTimeMillis();

                long res = result.CALLS_MODEXP;

                long leng = end - start;

                double resPL = res / Math.pow(a.length(), 3);
                
                System.out.println();
                
                System.out.println("Results for length " + a.length());
                System.out.println("Time: " + leng + " ms");
                System.out.println("Calls: " + res);
                System.out.println("Calls per length^3: " + resPL);
                
                outF.println(a.length()+","+leng);

            
            
            
        }
        outF.close();
        fw.close();

            } catch (IOException ex) {
                Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
            }



    }
}
